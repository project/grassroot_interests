Welcome to Grassroots.
As the name suggests get you the common interest out of specific keyword.

-- SUMMARY --

The GI Search menu module displays the extra panel at the top of
the search showing the common term or url you wish to focus 
when a certain "keyword" is searched. This module is not dependent
on search but it takes the argument passed in URL to match with the 
mannually entered keywords bind them with title having a URL or 
our choice within the site or some external URL.

Thus purpose of this module is to enhance user search experience.
By providing binding of as many keywords as we want to a URL. 

-- REQUIREMENTS --

Requires : Panels, ctools, page-manager


-- INSTALLATION --

* Install as usual, see http://drupal.org/node/70151 for further information.

-- CONFIGURATION --

* Configure user permissions in Administration » People » Permissions:

* When installed a seperate menu item can be seen in dashboard as 
  as GI - Search >> Add Keywords
  - Add keywords can be used to add the keyword, title and URLs.
* Setting up Panel page (adding plugin)
  - Move to : Structure >> Pages.
  - Create or enable the page you want to add GI results.
  - Move to contents tab.
  - Add Content > select Search Interest > GI - Your Interests.
  - Configure value on "Number of results to diaplay on match".
  - Set the text to be shown.
  - Hit Finish.
  - Save and update the panel page.
  - Done!


-- CONTACT --

Current maintainers:
* Raviish Gupta (ravyg) - http://drupal.org/user/1275746
* Arijit Dutta (fotuzlab) - http://drupal.org/user/886678
